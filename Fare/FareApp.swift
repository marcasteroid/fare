//
//  FareApp.swift
//  Fare
//
//  Created by Marco Margarucci on 08/01/21.
//

import SwiftUI

@main
struct FareApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
