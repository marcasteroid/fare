//
//  ThemeData.swift
//  Fare
//
//  Created by Marco Margarucci on 10/01/21.
//

import Foundation
import SwiftUI

// MARK: - Theme data

let data: [Theme] = [
    Theme(id: 0, name: "Pink", color: Color("Pink")),
    Theme(id: 1, name: "Green", color: Color("Green")),
    Theme(id: 2, name: "Blue", color: Color("Blue"))
]

