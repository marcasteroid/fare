//
//  ThemeSettings.swift
//  Fare
//
//  Created by Marco Margarucci on 10/01/21.
//

import Foundation
import SwiftUI

// MARK: - Theme class
final public class ThemeSettings: ObservableObject {
    // Theme settings
    @Published public var themeSettings: Int = UserDefaults.standard.integer(forKey: "Theme") {
        didSet {
            UserDefaults.standard.set(self.themeSettings, forKey: "Theme")
        }
    }
    
    private init() {}
    public static let shared = ThemeSettings()
}

