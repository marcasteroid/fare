//
//  ThemeModel.swift
//  Fare
//
//  Created by Marco Margarucci on 10/01/21.
//

import Foundation
import SwiftUI

// MARK: - Theme Model
struct Theme: Identifiable {
    // Id
    let id: Int
    // Name
    let name: String
    // Color
    let color: Color
}
