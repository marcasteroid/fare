//
//  FormRowLinkView.swift
//  Fare
//
//  Created by Marco Margarucci on 10/01/21.
//

import SwiftUI

struct FormRowLinkView: View {
    // MARK: - Properties
    // Icon
    var icon: String
    // Color
    var color: Color
    // Text
    var text: String
    // Link
    var link: String
    
    // MARK: - Body
    var body: some View {
        HStack {
            ZStack {
                RoundedRectangle(cornerRadius: 9, style: .continuous)
                    .fill(color)
                Image(systemName: icon)
                    .imageScale(.large)
                    .foregroundColor(Color.white)
            }
            .frame(width: 36, height: 36, alignment: .center)
            Text(text)
                .font(.custom("AvenirNext-DemiBold", size: 14))
                .foregroundColor(Color.gray)
            Spacer()
            Button(action: {
                guard let url = URL(string: self.link), UIApplication.shared.canOpenURL(url) else { return }
                UIApplication.shared.open(url as URL)
            }, label: {
                Image(systemName: "chevron.right")
                    .font(.system(size: 14, weight: .semibold, design: .rounded))
            })
            .accentColor(Color(.systemGray2))
        }
    }
}

// MARK: - Previews
struct FormRowLinkView_Previews: PreviewProvider {
    static var previews: some View {
        FormRowLinkView(icon: "globe", color: Color.pink, text: "Website", link: "www.apple.com")
            .previewLayout(.fixed(width: 375, height: 60))
            .padding()
    }
}
