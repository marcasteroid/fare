//
//  FormRowStaticView.swift
//  Fare
//
//  Created by Marco Margarucci on 10/01/21.
//

import SwiftUI

struct FormRowStaticView: View {
    // MARK: - Properties
    // Icon
    var icon: String
    // First text
    var firstText: String
    // Second text
    var secondText: String
    
    // MARK: - Body
    var body: some View {
        HStack {
            ZStack {
                RoundedRectangle(cornerRadius: 9, style: .continuous)
                    .fill(Color.gray)
                Image(systemName: icon)
                    .foregroundColor(Color.white)
            }
            .frame(width: 46, height: 46, alignment: .center)
            Text(firstText)
                .font(.custom("AvenirNext-DemiBold", size: 14))
                .foregroundColor(Color.gray)
            Spacer()
            Text(secondText)
                .font(.custom("AvenirNext-DemiBold", size: 14))
        }
        //.padding(.leading, 6)
        //.padding(.trailing, 6)
    }
}

// MARK: - Preview
struct FormRowStaticView_Previews: PreviewProvider {
    static var previews: some View {
        FormRowStaticView(icon: "gear", firstText: "Application", secondText: "Todo")
            .previewLayout(.fixed(width: 375, height: 60))
    }
}
