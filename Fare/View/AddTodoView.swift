//
//  AddTodoView.swift
//  Fare
//
//  Created by Marco Margarucci on 08/01/21.
//

import SwiftUI

struct AddTodoView: View {
    // MARK: - Properties
    // Theme
    @ObservedObject var theme = ThemeSettings.shared
    // Themes
    var themes: [Theme] = data
    // Todo name
    @State private var name: String = ""
    // Selected priority
    @State private var selectedPriority: Priority = .normal
    // Presentation mode
    @Environment(\.presentationMode) var presentationMode
    // Managed object context
    @Environment(\.managedObjectContext) private var managedObjectContext
    // Priorities
    enum Priority: String, Equatable, CaseIterable {
        case high = "High"
        case normal = "Normal"
        case low = "Low"
        
        var localizedName: LocalizedStringKey { LocalizedStringKey(rawValue) }
    }
    // Show error
    @State private var showError: Bool = false
    // Error title
    @State private var errorTitle: String = ""
    // Error message
    @State private var errorMessage: String = ""
    
    // MARK: - Body
    var body: some View {
        // MARK: - Navigation
        NavigationView {
            VStack {
                VStack(alignment: .leading, spacing: 20) {
                    // Todo name textfield
                    TextField("enter todo name here", text: $name)
                        .padding()
                        .background(Color(UIColor.tertiarySystemFill))
                        .cornerRadius(10)
                        .font(.custom("AvenirNext-DemiBold", size: 14))
                    // Todo priorities picker
                    Picker("Priority", selection: $selectedPriority) {
                        ForEach(Priority.allCases, id: \.self) { value in
                            Text(value.localizedName)
                                .font(.custom("AvenirNext-DemiBold", size: 14))
                                .tag(value)
                        }
                    }
                    .padding(.top, 16)
                    .pickerStyle(SegmentedPickerStyle())
                    .font(.custom("AvenirNext-DemiBold", size: 14))
                    // Save button
                    Button(action: {
                        if self.name != "" {
                            let todo = Todo(context: self.managedObjectContext)
                            todo.name = self.name
                            todo.priority = self.selectedPriority.rawValue
                            // Save todo
                            do {
                                try self.managedObjectContext.save()
                            } catch {
                                debugPrint("Error saving Todo")
                            }
                        } else {
                            self.showError = true
                            self.errorTitle = "Invalid name"
                            self.errorMessage = "Please enter valid name for the todo item"
                            return
                        }
                        self.presentationMode.wrappedValue.dismiss()
                    }, label: {
                        Text("SAVE")
                            .font(.custom("AvenirNext-DemiBold", size: 17))
                            .padding()
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .background(themes[self.theme.themeSettings].color)
                            .cornerRadius(10)
                            .foregroundColor(Color.white)
                    }) //: SAVE BUTTON
                } //: VSTACK
                .padding(.horizontal)
                .padding(.vertical, 30)
                Spacer()
            }
            .navigationBarTitle(Text("New Todo"), displayMode: .inline)
            .navigationBarItems(trailing: Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }, label: {
                Image(systemName: "xmark")
            }))
            // Present error alert
            .alert(isPresented: $showError) {
                Alert(title: Text(errorTitle).font(.custom("AvenirNext-DemiBold", size: 15)), message: Text(errorMessage).font(.custom("AvenirNext-DemiBold", size: 15)), dismissButton: .default(Text("OK").font(.custom("AvenirNext-DemiBold", size: 15))))
            }
        } // : NAVIGATION
        .accentColor(themes[self.theme.themeSettings].color)
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

// MARK: - Preview
struct AddTodoView_Previews: PreviewProvider {
    static var previews: some View {
        AddTodoView()
    }
}
