//
//  EmptyListView.swift
//  Fare
//
//  Created by Marco Margarucci on 09/01/21.
//

import SwiftUI

struct EmptyListView: View {
    // MARK: - Properties
    // Animation check variable
    @State private var isAnimated: Bool = false
    // Images
    let images: [String] = ["illustration-no1", "illustration-no2", "illustration-no3"]
    
    // MARK: - Body
    var body: some View {
        ZStack {
            VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 20, content: {
                Image("\(images.randomElement() ?? self.images[0])")
                    .resizable()
                    .scaledToFit()
                    .frame(minWidth: 256, idealWidth: 280, maxWidth: 360, minHeight: 256, idealHeight: 280, maxHeight: 360, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .layoutPriority(1)
                Text("All done for the day\nPlease tap the \"+\" button to add new todo")
                    .font(.custom("AvenirNext-DemiBold", size: 15))
                    .multilineTextAlignment(.center)
                    .layoutPriority(0.5)
            }) //: VSTACK
            .padding(.horizontal)
            .opacity(isAnimated ? 1 : 0)
            .offset(y: isAnimated ? 0 : -50)
            .animation(.easeOut(duration: 1.5), value: isAnimated)
            .onAppear(perform: {
                self.isAnimated.toggle()
            })
        } //: ZSTACK
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
        .background(Color("ColorBase"))
        .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
    }
}

// MARK: - Preview
struct EmptyListView_Previews: PreviewProvider {
    static var previews: some View {
        EmptyListView()
            .environment(\.colorScheme, .dark)
    }
}
