//
//  SettingsView.swift
//  Fare
//
//  Created by Marco Margarucci on 09/01/21.
//

import SwiftUI

struct SettingsView: View {
    // MARK: - Properties
    // Presentation mode
    @Environment(\.presentationMode) var presentationMode
    // Themes
    let themes: [Theme] = data
    // Theme
    @ObservedObject var theme = ThemeSettings.shared
    // Is theme changed
    @State private var isThemeChanged: Bool = false
    
    // MARK: - Body
    var body: some View {
        NavigationView {
            VStack(alignment: .center, spacing: 0) {
                // MARK: - Form
                Form {
                    // MARK: - Section 1
                    Section(header: HStack {
                        Text("Choose app theme").font(.custom("AvenirNext-DemiBold", size: 15))
                        Image(systemName: "circle.fill")
                            .resizable()
                            .frame(width: 12, height: 12)
                            .foregroundColor(themes[self.theme.themeSettings].color)
                    }) {
                        List {
                            ForEach(themes, id: \.id) { item in
                                Button(action: {
                                    self.theme.themeSettings = item.id
                                    UserDefaults.standard.set(self.theme.themeSettings, forKey: "Theme")
                                    self.isThemeChanged.toggle()
                                }) {
                                    HStack {
                                        Image(systemName: "circle.fill")
                                            .foregroundColor(item.color)
                                        Text(item.name)
                                            .font(.custom("AvenirNext-DemiBold", size: 15))
                                    }
                                } // : BUTTON
                                .accentColor(Color.primary)
                            }
                        }
                    } // : SECTION 1
                    .alert(isPresented: $isThemeChanged) {
                        Alert(title: Text("SUCCESS").font(.custom("AvenirNext-DemiBold", size: 15)), message: Text("Fare has been changed to the \(themes[self.theme.themeSettings].name) color theme").font(.custom("AvenirNext-DemiBold", size: 15)), dismissButton: .default(Text("OK").font(.custom("AvenirNext-DemiBold", size: 15))))
                    }
                    .padding(.vertical, 3)
                    // MARK: - Section 2
                    Section(header: Text("Follow us on social media").font(.custom("AvenirNext-DemiBold", size: 15))) {
                        FormRowLinkView(icon: "globe", color: Color.pink, text: "Website", link: "https://www.apple.com")
                        FormRowLinkView(icon: "florinsign.circle.fill", color: Color.blue, text: "Facebook", link: "https://www.facebook.com")
                    } // : SECTION 2
                    .padding(.vertical, 3)
                    // MARK: - Section 3
                    Section(header: Text("About the application").font(.custom("AvenirNext-DemiBold", size: 15))) {
                        FormRowStaticView(icon: "gear", firstText: "Application", secondText: "Fare")
                        FormRowStaticView(icon: "checkmark.seal", firstText: "Compatibility", secondText: "iPhone, iPad")
                        FormRowStaticView(icon: "keyboard", firstText: "Developer", secondText: "Marco Margarucci")
                        FormRowStaticView(icon: "paintbrush", firstText: "Designer", secondText: "Marco Margarucci")
                        FormRowStaticView(icon: "flag", firstText: "Version", secondText: "0.0.1")
                    } //: SECTION 3
                    .padding(.vertical, 3)
                } //: FORM
                .listStyle(GroupedListStyle())
                .environment(\.horizontalSizeClass, .regular)
                // MARK: - Footer
                Text("Copyright © All right reserved\nmarcasteroid")
                    .font(.custom("AvenirNext-DemiBold", size: 14))
                    .multilineTextAlignment(.center)
                    .padding(.top, 6)
                    .padding(.bottom, 30)
                    .foregroundColor(Color.secondary)
            } //: VSTACK
            .navigationBarItems(trailing: Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }, label: {
                Image(systemName: "xmark")
            }))
            .navigationBarTitle(Text("Settings"), displayMode: .inline)
            .background(Color("ColorBackground"))
            .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
        } //: NAVIGATION
        .accentColor(themes[self.theme.themeSettings].color)
    }
}

// MARK: - Preview
struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
