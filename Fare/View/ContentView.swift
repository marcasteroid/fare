//
//  ContentView.swift
//  Fare
//
//  Created by Marco Margarucci on 08/01/21.
//

import SwiftUI
import CoreData

struct ContentView: View {
    // MARK: - Properties
    // Theme
    @ObservedObject var theme = ThemeSettings.shared
    // Themes
    var themes: [Theme] = data
    // Show add todo view
    @State private var showAddTodoView: Bool = false
    // Show settings view
    @State private var showSettingsView:Bool = false
    // Animating button
    @State private var animatingButton: Bool = false
    // Managed object context
    @Environment(\.managedObjectContext) private var managedObjectContext
    // Fetch request
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \Todo.name, ascending: true)],
        animation: .default)
    private var todos: FetchedResults<Todo>

    var body: some View {
        // Navigation view
        NavigationView {
            // List
            ZStack {
                List {
                    ForEach(self.todos, id: \.self) { todo in
                        HStack {
                            Circle()
                                .frame(width: 12, height: 12, alignment: .center)
                                .foregroundColor(self.colorize(priority: todo.priority))
                            Text(todo.name)
                                .font(.custom("AvenirNext-DemiBold", size: 15))
                            Spacer()
                            Text(todo.priority)
                                .font(.custom("AvenirNext-DemiBold", size: 15))
                                .foregroundColor(Color(UIColor.systemGray2))
                                .padding(3)
                                .frame(minWidth: 63)
                                .overlay(Capsule().stroke(Color(UIColor.systemGray2), lineWidth: 0.75))
                        } //: HSTACK
                        .padding(.vertical, 10)
                    }
                    .onDelete(perform: delete)
                } //: LIST
                .navigationBarTitle(Text("Todo"), displayMode: .inline)
                .navigationBarItems(
                    leading: EditButton().accentColor(themes[self.theme.themeSettings].color),
                    trailing: Button(action: {
                        self.showSettingsView.toggle()
                    }) {
                        Image(systemName: "gearshape")
                    }
                    .accentColor(themes[self.theme.themeSettings].color)
                    .sheet(isPresented: $showSettingsView) {
                        SettingsView()
                    }
                )
                // MARK: - No todo items
                if todos.count == 0 {
                    EmptyListView()
                }
            } //: ZSTACK
            .sheet(isPresented: $showAddTodoView) {
                AddTodoView().environment(\.managedObjectContext, self.managedObjectContext)
            }
            .overlay(
                ZStack {
                    Group {
                        Circle()
                            .fill(themes[self.theme.themeSettings].color)
                            .opacity(self.animatingButton ? 0.2 : 0)
                            .scaleEffect(self.animatingButton ? 1 : 0)
                            .frame(width: 68, height: 68, alignment: .center)
                        Circle()
                            .fill(themes[self.theme.themeSettings].color)
                            .opacity(self.animatingButton ? 0.15 : 0)
                            .scaleEffect(self.animatingButton ? 1 : 0)
                            .frame(width: 88, height: 88, alignment: .center)
                    }
                    .animation(Animation.easeInOut(duration: 2).repeatForever(autoreverses: true), value: animatingButton)
                    
                    Button(action: {
                        self.showAddTodoView.toggle()
                    }) {
                        Image(systemName: "plus.circle.fill")
                            .resizable()
                            .scaledToFit()
                            .background(Circle().fill(Color("ColorBase")))
                            .frame(width: 48, height: 48, alignment: .center)
                    } //: BUTTON
                    .accentColor(themes[self.theme.themeSettings].color)
                    .onAppear(perform: {
                        self.animatingButton.toggle()
                    })
                } //: ZSTACK
                .padding(.bottom, 15)
                .padding(.trailing, 15)
                , alignment: .bottomTrailing
            )
        } //: NAVIGATIONVIEW
        .navigationViewStyle(StackNavigationViewStyle())
    }
    
    // MARK: - Functions
    
    // Delete todo
    private func delete(at offsets: IndexSet) {
        for index in offsets {
            let todo = todos[index]
            managedObjectContext.delete(todo)
            do {
                try managedObjectContext.save()
            } catch {
                debugPrint("Error deleting todo: \(error)")
            }
        }
    }
    
    
    private func colorize(priority: String) -> Color {
        switch priority {
        case "High":
            return Color("Pink")
        case "Normal":
            return Color("Green")
        case "Low":
            return Color("Blue")
        default:
            return .gray
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
